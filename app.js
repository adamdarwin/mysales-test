const express = require('express'),
    socket = require('socket.io'),
    http = require('http');

require('dotenv').config();

const LinkController = require('./controllers/Link');

let app = express();

app.use(express.static('public'));


app.set('views', './views');
app.set('view engine', 'pug');

const PORT = 3000;

let server = http.Server(app).listen(PORT, () =>{
    console.log('Server up on ', PORT,' port');
});

let io = socket(server);

io.on('connection', socket => {
    socket.on('getCost', async links => {
        io.emit('sendCost', await LinkController.getCost(links));
    });
});

app.all('/', require ('./routes/main')(io));
