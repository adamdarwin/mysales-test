const puppeteer = require('puppeteer'),
    cheerio = require('cheerio');
    path = require('path');

module.exports = class Link {

    /*
    * Get cost for given links
    *
    * @param    {array} links   Array of links to serve
    *
    * @return   {array} Array with product information {url, name, cost}
    * */

    static async getCost(links){
        try {
            const browser = await puppeteer.launch();
            let pages = [];
            for (let i = 0; i < links.length; i++) {
                if(links[i].indexOf('https://rozetka.com.ua/') !== 0){
                    pages.push(false);
                    continue;
                }
                const page = await browser.newPage();
                await page.goto(links[i]);
                let html = await page.evaluate(() => document.body.innerHTML);
                let $ = cheerio.load(html);
                pages.push({url : links[i],name : $('h1').text(), cost : + $('.detail-price-uah').contents().first().text().replace(' ','')});
                await page.pdf({path: path.join(__dirname, '../public/pages/' + (links[i].replace('https://rozetka.com.ua/', '')).replace(/\//g, '_') + '.pdf'), format: 'A4'});
            }
            await browser.close();
            return pages;
        }catch (e) {
            console.log(e.message);
            return false;
        }
    }
};