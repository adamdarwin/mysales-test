let socket = io('http://localhost:3000');

$(document).ready(() => {
    socket.on('sendCost', res => {
        $('#main-table tbody').empty();
        $('#main-spinner').css('display','none');
        $('#main-table').css('display','block');
        for (let i = 0; i < res.length; i++) {
            if(!res[i] || !res[i].cost || res[i].name === '')continue;
            $('#main-table > tbody').append('<tr><th>' + res[i].url + '</th><th>' + res[i].name + '</th><th>' + res[i].cost + '</th></tr>');
        }
        return $('#submit').attr('disabled', false);
    });

    $('#submit').click((e) => {
        e.preventDefault();
        $('#main-table').css('display','none');
        let links = (($('#link-area').val()).split('\n')).filter(val => val !== '');
        // let links = ($('#link-area').attr('placeholder')).split('\n');
        if(links.length < 3 || links.length > 5)return alert('Invalid aunt of links: ' + links.length);
        $('#main-spinner').css('display','block');
        $('#submit').attr('disabled', true);
        return socket.emit('getCost', links);
    });
});