const express = require('express'),
    router = express.Router();

module.exports = function (io) {
    router.get('/', async (req, res) => {
       try {
           return res.render('index', {title : 'Main'})
       }catch (e) {
           console.log('/', e.message);
           return res.sendStatus(500);
       }
    });
    return router;
};